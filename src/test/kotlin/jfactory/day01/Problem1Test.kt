package jfactory.day01

import jfactory.util.resourceFile
import org.junit.jupiter.api.*

class Problem1Test {

    private fun readFrequencyShifts() = resourceFile("day1/part1.txt").readLines().map { it.toInt() }

    @Test
    fun solvePart1(){
        println("Sum of Shifts: ${readFrequencyShifts().sum()}")
    }

    private val frequencyShiftData = listOf(
        listOf(1,-1) to 0,
        listOf(+3, +3, +4, -2, -4) to 10,
        listOf(-6, +3, +8, +5, -6) to 5,
        listOf(+7, +7, -2, -7, -4) to 14
        )

    @TestFactory
    @DisplayName("Part 2: Test Scenarios")
    fun testPart2() = frequencyShiftData
        .map { (input, expected) ->
            DynamicTest.dynamicTest("when I given frequency shifts $input then first repeated frequency is $expected") {
                Assertions.assertEquals(expected, firstRepeatedFrequency(input))
            }
        }

    @Test
    fun solvePart2(){
        val frequencyShifts = readFrequencyShifts()
        val firstRepeat = firstRepeatedFrequency(frequencyShifts)
        println("First Repeated Frequency: $firstRepeat")
    }

}