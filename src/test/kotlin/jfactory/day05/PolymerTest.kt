package jfactory.day05

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class PolymerTest {

    @Test
    fun part1Test() {
        assertEquals("dabCBAcaDA", part1("dabAcCaCBAcCcaDA"))
    }

    @Test
    fun part2Test() {
        assertEquals("daDA", part2("dabAcCaCBAcCcaDA"))
    }
}