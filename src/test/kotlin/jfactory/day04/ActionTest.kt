package jfactory.day04

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class ActionTest {

    @Test
    fun testParseChangeGuard(){
        val action = Action.parse("[1518-04-19 23:48] Guard #773 begins shift")
        assertNotNull(action)
        assertTrue(action is GuardChange)
        val guardChange = action as GuardChange
        assertEquals(773, guardChange.guardNo)
        assertEquals(48, guardChange.minute)
        assertEquals(23, guardChange.hour)
    }

    @Test
    fun testParseSleep(){
        val action = Action.parse("[1518-07-12 00:54] falls asleep")
        assertNotNull(action)
        assertTrue(action is FallAsleep)
        val fallAsleep = action as FallAsleep
        assertEquals(54, fallAsleep.minute)
        assertEquals(0, fallAsleep.hour)
    }

    @Test
    fun testParseWake(){
        val action = Action.parse("[1518-09-18 00:58] wakes up")
        assertNotNull(action)
        assertTrue(action is WakeUp)
        val wakeUp = action as WakeUp
        assertEquals(58, wakeUp.minute)
        assertEquals(0, wakeUp.hour)
    }

}