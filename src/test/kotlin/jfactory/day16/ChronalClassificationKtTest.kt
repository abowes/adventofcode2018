package jfactory.day16

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class ChronalClassificationOperationsTest {

    @Test
    fun testAddr() {
        val registers : Registers = mutableListOf(0,4,12,0)
        assertEquals(listOf(0,4,12,16), registers.addr(listOf(13,1,2,3)) )
    }

    @Test
    fun testAddi() {
        val registers : Registers = mutableListOf(0,4,12,0)
        assertEquals(listOf(0,4,12,6), registers.addi(listOf(13,1,2,3)) )
    }

    @Test
    fun testMulr() {
        val registers : Registers = mutableListOf(0,4,12,0)
        assertEquals(listOf(0,4,12,48), registers.mulr(listOf(13,1,2,3)) )
    }

    @Test
    fun testMuli() {
        val registers : Registers = mutableListOf(0,4,12,0)
        assertEquals(listOf(0,4,12,8), registers.muli(listOf(13,1,2,3)) )
    }

    @Test
    fun testBanr() {
        val registers : Registers = mutableListOf(0,28,55,0)
        assertEquals(listOf(0,28,55,20), registers.banr(listOf(13,1,2,3)) )
    }

    @Test
    fun testBani() {
        val registers : Registers = mutableListOf(0,29,55,0)
        assertEquals(listOf(0,29,55,21), registers.bani(listOf(13,1,55,3)) )
    }

    @Test
    fun testBorr() {
        val registers : Registers = mutableListOf(0,28,55,0)
        assertEquals(listOf(0,28,55,63), registers.borr(listOf(13,1,2,3)) )
    }

    @Test
    fun testBori() {
        val registers : Registers = mutableListOf(0,28,17,0)
        assertEquals(listOf(0,28,17,63), registers.bori(listOf(13,1,55,3)) )
    }

    @Test
    fun testSetr() {
        val registers : Registers = mutableListOf(0,28,17,0)
        assertEquals(listOf(0,28,17,28), registers.setr(listOf(13,1,0,3)) )
    }

    @Test
    fun testSeti() {
        val registers : Registers = mutableListOf(0,28,17,0)
        assertEquals(listOf(0,28,17,1), registers.seti(listOf(13,1,0,3)) )
    }

    @Test
    fun testGtir() {
        val registers : Registers = mutableListOf(0,28,2,0)
        assertEquals(listOf(0,28,2,0), registers.gtir(listOf(13,1,2,3)) )
        assertEquals(listOf(0,28,2,1), registers.gtir(listOf(13,3,2,3)) )
    }

    @Test
    fun testGtri() {
        val registers : Registers = mutableListOf(0,2,17,0)
        assertEquals(listOf(0,2,17,0), registers.gtri(listOf(13,1,2,3)) )
        assertEquals(listOf(0,2,17,1), registers.gtri(listOf(13,1,1,3)) )
    }

    @Test
    fun testGtrr() {
        val registers : Registers = mutableListOf(0,28,17,0)
        assertEquals(listOf(0,28,17,0), registers.gtrr(listOf(13,2,1,3)) )
        assertEquals(listOf(0,28,17,1), registers.gtrr(listOf(13,1,2,3)) )
    }

    @Test
    fun testEqir() {
        val registers : Registers = mutableListOf(0,5,4,0)
        assertEquals(listOf(0,5,4,0), registers.eqir(listOf(13,5,2,3)) )
        assertEquals(listOf(0,5,4,0), registers.eqir(listOf(13,3,2,3)) )
        assertEquals(listOf(0,5,4,1), registers.eqir(listOf(13,5,1,3)) )
    }

    @Test
    fun testEqri() {
        val registers : Registers = mutableListOf(0,5,4,0)
        assertEquals(listOf(0,5,4,0), registers.eqri(listOf(13,1,4,3)) )
        assertEquals(listOf(0,5,4,0), registers.eqri(listOf(13,1,6,3)) )
        assertEquals(listOf(0,5,4,1), registers.eqri(listOf(13,1,5,3)) )
    }

    @Test
    fun testEqrr() {
        val registers : Registers = mutableListOf(0,5,4,5)
        assertEquals(listOf(0,5,1,5), registers.eqrr(listOf(13,1,3,2)) )
        assertEquals(listOf(0,5,1,0), registers.eqrr(listOf(13,1,2,3)) )
    }

}


internal class ChronalClassificationTestCaseTest {

    @Test
    fun testIdentifyMappings() {
        val testCase = TestCase(listOf(3, 2, 1, 1), listOf(9,2,1,2), listOf(3, 2, 2, 1))
        assertEquals(3, testCase.countMatchingOpCodes())
        assertEquals(setOf("mulr","addi","seti").sorted(), testCase.matchingOpCodes().sorted())
    }

}