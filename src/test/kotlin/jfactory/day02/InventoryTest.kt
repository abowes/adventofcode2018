package jfactory.day02

import jfactory.util.hammingDistance
import jfactory.util.resourceFile
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class InventoryTest{

    val productIds = resourceFile("day02/productIds.txt").readLines()

    @Test
    fun `Given a list of Products Then correct checksum is generated`(){
        assertEquals(12, listOf("abcdef","abbcde","abcccd","bababc","aabcdd","abcdee","ababab").productChecksum())
    }

    @Test
    fun solvePart1(){
        println( productIds.productChecksum())
    }

    @Test
    fun `Given 2 Strings The correct Hamming Distance is returned`(){
        assertEquals(2, "abcde".hammingDistance("axcye"))
    }

    @Test
    fun `Given a list of products Then strings with a distance of 1 are returned`(){
        val match = listOf("abcde", "fghij", "klmno", "pqrst", "fguij", "axcye", "wvxyz").findCloseMatch()
        assertEquals("fghij", match.first)
        assertEquals("fguij", match.second)
    }

    @Test
    fun solvePart2(){
        println( productIds.findCloseMatch().removeMismatches())
    }

}



