package jfactory.day12

import jfactory.util.resourceFile
import kotlin.system.measureTimeMillis

fun evolve(plants : String, startPos:Int, rules: Map<String, Char>) : Pair<String, Int>{
    val pots = "..$plants...."
    val next = (2 until pots.length-2).map{ rules.getOrDefault(pots.slice(it-2..it+2),'.')}
        .joinToString("").dropLastWhile { it == '.' }
    return next to startPos
}

fun score(pots: String, startPos: Int) : Int {
    return pots.mapIndexed { i, c -> if (c == '#') i + startPos else 0 }.sum()
}

fun solve(initialState: String, rules : Map<String,Char>): Pair<Int,Long>{

    val (finalPots, startPos) = (1..20).fold("..$initialState" to -2){
        state, _-> evolve(state.first, state.second, rules)
    }

    val part1 = score(finalPots, startPos)

    // Pattern reaches 'Steady State' after less than 120 iterations & just glides 1 step to the right each step
    // Each alive pot in the pattern then adds 1 to the score per iteration.
    val (finalPots2, startPos2) = (1..120).fold("..$initialState" to -2){
            state, _-> evolve(state.first, state.second, rules)
    }
    val part2  = ((50000000000 - 120) * finalPots2.count { it == '#' }) + score(finalPots2, startPos2)

    return part1 to part2
}

fun main(args: Array<String>) {
    val time = measureTimeMillis {
        val lines = resourceFile("day12/input.txt").readLines()
        val initialState = lines[0].substring(14).trim()
        val rules = lines.drop(2).map{ it.subSequence((0..4)).toString() to it.last()}.toMap()

        val (part1,part2) = solve(initialState,rules)

        println("Part I: $part1")
        println("Part II: $part2")

    }
    println("Elapsed Time: ${time}ms")
}