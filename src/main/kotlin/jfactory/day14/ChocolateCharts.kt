package jfactory.day14

import java.util.*
import kotlin.system.measureTimeMillis


fun generateScores()= sequence {
    val scoreBoard = mutableListOf(3,7)
    yield(3)
    yield(7)
    var ptr1 = 0
    var ptr2 = 1
    while (true){
        val newScore = scoreBoard[ptr1] + scoreBoard[ptr2]
        newScore.toString().map { it.toString().toInt()}.forEach {
            scoreBoard.add(it)
            yield(it)
        }
        ptr1 = (ptr1+ 1 + scoreBoard[ptr1]) % scoreBoard.size
        ptr2 = (ptr2+ 1 + scoreBoard[ptr2]) % scoreBoard.size
    }
}

fun part2(n:Int): Int{
    val target = n.toString().map { it.toString().toInt() }
    val iter = generateScores().iterator()
    val window = LinkedList((0 until target.size).map { iter.next() })
    var i = 0
    while (window != target){
        i++
        window.add(iter.next())
        window.removeAt(0)
    }
    return i
}

fun solve(n:Int):Pair<String,Int>{
    val part1 = generateScores().drop(n).take(10).joinToString("")
    return part1 to part2(n)
}

fun main(args: Array<String>) {
    val time = measureTimeMillis {
        val n = 330121
        println(solve(n))
    }
    println("Elapsed Time: ${time}ms")
}