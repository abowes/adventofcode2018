package jfactory.day02

import jfactory.util.hammingDistance
import jfactory.util.resourceFile

fun List<String>.productChecksum(): Int {
    val letterCounts = this.map { word -> word.groupBy { it }.map { it.value.size } }
    return letterCounts.count { it.contains(2) } * letterCounts.count { it.contains(3) }
}

fun List<String>.findCloseMatch(): Pair<String, String> {
    return sequence {
        for (x in 0 until this@findCloseMatch.size - 1) {
            for (y in x until this@findCloseMatch.size) {
                yield(this@findCloseMatch[x] to this@findCloseMatch[y])
            }
        }
    }.first { it.first.hammingDistance(it.second) == 1 }
}

fun Pair<String, String>.removeMismatches() = first.removeMismatches(second)

fun String.removeMismatches(other: String) =
    this.zip(other)
        .filter { it.first == it.second }
        .joinToString(separator = "", transform = { "${it.first}" })


fun main(args: Array<String>) {
    val productIds = resourceFile("day02/productIds.txt").readLines()
    println( "Part 1: ${productIds.productChecksum()}")
    println( "Part 2: ${productIds.findCloseMatch().removeMismatches()}")
}