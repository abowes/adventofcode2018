package jfactory.day03

import jfactory.util.resourceFile
import kotlin.system.measureTimeMillis

data class Claim(val id: Int, val x: Int, val y: Int, val width: Int, val height: Int) {
    val x2 = x + width
    val y2 = y + height

    fun area(): Sequence<Pair<Int, Int>> = sequence {
        for (y in y until y2) {
            for (x in x until x2) {
                yield(x to y)
            }
        }
    }

    companion object {
        private val CLAIM_REGEX = Regex("""^#(\d*) @ (\d*),(\d*): (\d*)x(\d*)$""")
        fun parse(input: String): Claim {
            val (id, x, y, width, height) = CLAIM_REGEX.matchEntire(input)!!
                .groupValues.drop(1)
                .map(String::toInt)
            return Claim(id, x, y, width, height)
        }
    }
}

typealias Cloth = Array<Array<Int>>
operator fun Cloth.plus(cell: Pair<Int, Int>) = this[cell.second][cell.first]++

fun List<Claim>.toCloth(): Cloth {
    val clothWidth = this.maxBy { it.x2 }!!.x2
    val clothHeight = this.maxBy { it.y2 }!!.y2

    val cloth2 = Array(clothHeight) { Array(clothWidth) { 0 } }
    this.forEach { claim -> claim.area().forEach { cloth2 + it } }

    return cloth2
}

/**
 * Check if the Claim has fitted into the Cloth without overlap
 */
fun Claim.fits(cloth: Array<Array<Int>>) = this.area().all { cloth[it.second][it.first] == 1 }

fun main(args: Array<String>) {
    val time = measureTimeMillis {
        val claims = resourceFile("day03/claims.txt").readLines().map { Claim.parse(it) }
        val cloth = claims.toCloth()
        println(cloth.sumBy { it.count { it > 1 } })
        println(claims.first { it.fits(cloth) })

    }
    println("Elapsed Time: ${time}ms")
}