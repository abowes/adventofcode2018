package jfactory.day01

import jfactory.util.cycle
import jfactory.util.accumulate

fun <E> Sequence<E>.firstRepeatedElement(initalValue:E): E {
    val prevFrequencies = mutableSetOf(initalValue)
    return this.first { !prevFrequencies.add(it) }
}

fun firstRepeatedFrequency(frequencyShifts: List<Int>): Int {
    return frequencyShifts.cycle()
        .accumulate(0){acc:Int, element: Int -> acc + element}
        .firstRepeatedElement(0)
}
