package jfactory.day21


fun main(args: Array<String>) {
    println("Part I : ${simulator(2176960)}")
    println("Part II : ${simulator(2176960, false)}")
}


fun simulator(magicNumber: Int, isPart1: Boolean = true): Int {
    val seen = mutableListOf<Int>()
    var c = 0
    while (true) {
        var a = c or 65536
        c = magicNumber
        while (true) {
            c = (((c + (a and 255)) and 16777215) * 65899) and 16777215
            if (256 > a) {
                if (isPart1) {
                    return c
                } else {
                    if (c !in seen) {
                        seen.add(c)
                        break
                    } else {
                        return seen.last()
                    }
                }
            }
            else {
                a = Math.floorDiv(a, 256)
            }
        }
    }
}