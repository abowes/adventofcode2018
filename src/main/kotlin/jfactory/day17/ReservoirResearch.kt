package jfactory.day17

import jfactory.util.resourceFile
import kotlin.system.measureTimeMillis

data class Coord(val x:Int, val y:Int){
    operator fun minus(i:Int) = Coord(x-i,y)
    operator fun plus(i:Int) = Coord(x+i,y)
    fun below() = Coord(x,y+1)
    fun above() = Coord(x,y-1)
}

const val EMPTY = '.'
const val WALL = '#'
const val WATER = '~'
const val FALLING = '|'
const val FLOWING = '='
const val WATER_SOURCE = '+'

class Reservoir(width:Int=3000, height:Int=2000){

    private val elements: Array<CharArray> = (0..height).map { ("$EMPTY".repeat(width).toCharArray()) }.toTypedArray()
    private var top = 0
    private var bottom = elements.size

    operator fun get(coord: Coord) = elements[coord.y][coord.x]
    operator fun set(coord: Coord, v: Char){elements[coord.y][coord.x] = v}

    companion object {
        fun parse(lines: List<String>) : Reservoir {
            val reservoir = Reservoir()
            lines.forEach { line ->
                val n = line.substringAfter("=").substringBefore(",").toInt()
                val start = line.substringAfterLast("=").substringBefore("..").toInt()
                val finish = line.substringAfter("..").toInt()
                val range = start..finish
                when (line.first()) {
                    'x' -> range.forEach { y -> reservoir[Coord(n,y)] = WALL }
                    'y' -> range.forEach { x -> reservoir[Coord(x,n)] = WALL }
                }
            }
            return reservoir
        }
    }

    fun fill(){
        val sourceLocation = Coord(500, 0)
        this[sourceLocation] = WATER_SOURCE
        top = elements.indexOfFirst { it.any { c-> c == WALL } }
        bottom = elements.indexOfLast { it.any { c-> c == WALL } }
        fillFrom(sourceLocation)
    }

    private fun fillFrom(coord:Coord) : Boolean {
        val (flowElements, finished) = fallFrom(coord)
        return when {
            finished -> true
            flowElements.isEmpty() -> false
            else -> {
                val level : Coord? = flowElements.reversed().firstOrNull{
                    this[it] = WATER
                    val left = flowLeft(it)
                    val right = flowRight(it)
                    left || right
                }
                if (level != null ){
                    generateSequence(level){c -> c + 1}.takeWhile { this[it] in wetElements }.forEach { this[it] = FLOWING }
                    generateSequence(level){c -> c - 1}.takeWhile { this[it] in wetElements }.forEach { this[it] = FLOWING }
                    true
                } else {
                    false
                }
            }
        }
    }

    private fun fallFrom(coord: Coord): Pair<List<Coord>, Boolean> {
        var currentPos = coord.below()
        val falling = mutableListOf<Coord>()
        while (currentPos.y <= bottom){
            when (this[currentPos]){
                WALL, WATER -> return falling to false
                FLOWING     -> return emptyList<Coord>() to true
                else -> {
                    falling.add(currentPos)
                    this[currentPos] = FALLING
                }
            }
            currentPos = currentPos.below()
        }
        return emptyList<Coord>() to true
    }

    fun flowLeft(coord: Coord) = flow(coord){c-> c-1}
    fun flowRight(coord: Coord) = flow(coord){c-> c+1}

    private tailrec fun flow(coord: Coord, move: (Coord) -> Coord) : Boolean{
        if (this[coord] == WALL) return false
        this[coord] = WATER
        when (this[coord.below()]){
            EMPTY, FALLING -> {
                this[coord] = FALLING
                if (fillFrom(coord)) return true}
            FLOWING -> return true
        }
        return flow(move(coord), move)
    }

    val wetElements = listOf(WATER, FALLING, FLOWING)
    fun wetArea() = elements.sliceArray(top..bottom).sumBy { it.count { c-> c in wetElements } }
    fun stillArea() = elements.sliceArray(top..bottom).sumBy { it.count { c-> c == WATER } }

    fun print() = (0..bottom).forEach { y->
        println(elements[y].slice(400..600).joinToString(""))
    }
}


fun solve(reservoir: Reservoir) : Pair<Int,Int> {
    reservoir.fill()
    val part1 = reservoir.wetArea()
    return part1 to reservoir.stillArea()
}


fun main(args: Array<String>) {

    val time = measureTimeMillis {
        val reservoir = Reservoir.parse(resourceFile("day17/input.txt").readLines())
        val (part1, part2) = solve(reservoir)

    //    reservoir.print()
        println("Part I: $part1")
        println("Part II: $part2")
    }
    println("Elapsed Time ${time}ms")
}
