package jfactory.day11

import jfactory.util.accumulate
import kotlin.system.measureTimeMillis

fun rackId(x: Int) = x + 10
fun gridCharge(x: Int, y: Int, serialNumber: Int) = when {
    x == 0 -> 0
    y == 0 -> 0
    else -> (((rackId(x) * y + serialNumber) * rackId(x)) % 1000).div(100) - 5
}

typealias Grid = List<List<Int>>

fun buildGrid(serialNumber: Int): Grid = (0..300).map { y -> (0..300).map { x -> gridCharge(x, y, serialNumber) } }

private fun part2(grid: Grid, maxLength: Int = 60): Pair<Triple<Int, Int, Int>, Int> {
    val summedArea = grid.toSummedArea()
    val maxSquare = (1..297).map { y ->
        val rowMax = (1..297).map { x ->
            val maxScore = (2..minOf(300 - x, 300 - y, maxLength)
                    ).map { size -> size to summedArea.area( x, y, size, size) }.maxBy { it.second }!!
            Pair(x, maxScore.first) to maxScore.second
        }.maxBy { it.second }!!
        Triple(rowMax.first.first, y, rowMax.first.second) to rowMax.second
    }.maxBy { it.second }!!
    return maxSquare
}

private fun List<List<Int>>.area(x: Int, y: Int, width: Int, height: Int) =
    this[y + height - 1][x + width - 1] - this[y + height - 1][x - 1] - this[y - 1][x + width - 1] + this[y - 1][x - 1]

fun Grid.toSummedArea(): List<List<Int>> {
    val firstRow = this[0].asSequence().accumulate(0) { acc, x -> acc + x }.toList()
    val ret = this.drop(1).asSequence().accumulate(firstRow) { acc, row ->
        acc.zip(row.asSequence().accumulate(0) { rowTotal, i -> rowTotal + i }.toList()).map { it.first + it.second }
    }.toList()
    return listOf(listOf(firstRow), ret).flatten()
}

fun solve(serialNumber: Int): Pair<Triple<Int, Int, Int>, Triple<Int, Int, Int>> {
    val grid = buildGrid(serialNumber)
    val part1 = part2(grid,3)
    val maxSquare = part2(grid)

    return part1.first to maxSquare.first
}

fun main(args: Array<String>) {
    val time = measureTimeMillis {
        val serialNumber = 9810
        val (part1, part2) = solve(serialNumber)
        println(part1)
        println(part2)
    }
    println("Elapsed Time: ${time}ms")
}