package jfactory.day08

import jfactory.util.resourceFile

class Node(childNodes: List<Node>, metadata: List<Int>) {

    val totalPart1 : Int by lazy{ metadata.sum() + childNodes.sumBy { it.totalPart1 } }
    val totalPart2 : Int by lazy{
        when (childNodes.size){
            0 -> metadata.sum()
            else -> metadata.filter { it in (1 .. childNodes.size) }.sumBy{ childNodes[it-1].totalPart2 }
        }
    }

    companion object {
        fun parse(iterator: Iterator<Int>): Node {
            val childCount = iterator.next()
            val metaDataCount = iterator.next()

            val childNodes = (0 until childCount).map { Node.parse(iterator) }
            val metadata = (0 until  metaDataCount).map { iterator.next() }
            return Node(childNodes, metadata)
        }
    }
}

fun solve(input:List<Int>): Pair<Int,Int>{
    val root = Node.parse(input.listIterator())
    return Pair(root.totalPart1, root.totalPart2)
}


fun main(args: Array<String>) {
    val input = resourceFile("day08/input.txt").readLines().first().split(" ").map(String::toInt)
    val (part1, part2) = solve(input)
    println("Part I: $part1")
    println("Part II: $part2")
}