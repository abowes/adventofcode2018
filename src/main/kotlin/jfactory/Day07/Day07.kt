package jfactory.Day07

import jfactory.util.removeItem
import jfactory.util.resourceFile

class Tree<V : Comparable<V>>(links: List<Pair<V, V>>) {

    private val parentToChildren = links.groupBy({it.first},{it.second})
    private val childToParents = links.groupBy({it.second},{it.first})
        .map { it.key to it.value.toMutableList() }.toMap().toMutableMap()

    fun walkTree(workers: Int = 1, delay: (c: V) -> Int = { 0 }): Sequence<Pair<V, Int>> {

        val available = parentToChildren.keys.minus(childToParents.keys).map { it to 0 }.toMutableList()

        fun nextAvailable(): Pair<V, Int> {
            available.sortWith(compareBy({ it.second }, { it.first }))
            return available.removeAt(0)
        }

        fun processNode(c: V, finish: Int) {
            parentToChildren[c]?.let { children ->
                children.forEach { child ->
                    childToParents.removeItem(child, c)
                    if (child !in childToParents) {
                        available.add(child to finish)
                    }
                }
            }
        }

        val workerStarts = (0 until workers).map { 0 }.toIntArray()
        return sequence {
            while (available.isNotEmpty()) {
                val nextWorkerStart = workerStarts.min()!!
                val nextWorker = workerStarts.find { it == nextWorkerStart }!!
                val (c, availableAt) = nextAvailable()
                val finish = maxOf(nextWorkerStart, availableAt) + delay(c)
                workerStarts[nextWorker] = finish
                yield(Pair(c, finish))

                // Check if any of the Children are now available to be processed
                processNode(c, finish)
            }
        }

    }

}

fun solve(steps: List<Pair<Char, Char>>): Pair<String, Int> {
    val part1 = Tree(steps).walkTree().map { it.first }.joinToString("")
    val part2 = Tree(steps).walkTree(5) { c -> c.toInt() - 4 }.map { it.second }.max()!!
    return part1 to part2
}

@Suppress("LocalVariableName")
fun main(args: Array<String>) {
    val STEP_REGEX = Regex("""^Step (.) must be finished before step (.) can begin.""")

    val steps = resourceFile("day07/input.txt").readLines().map {
        val (_, start, finish) = STEP_REGEX.matchEntire(it)!!.groupValues
        start[0] to finish[0]
    }

    val (part1, part2) = solve(steps)

    println("Part I: $part1")
    println("Part II: $part2")
}