package jfactory.day04

import jfactory.util.resourceFile
import java.lang.IllegalArgumentException
import kotlin.system.measureTimeMillis

sealed class Action(val date: String, val hour: Int, val minute: Int) {
    companion object {
        fun parse(input: String): Action {
            return when {
                input.endsWith("begins shift") -> GuardChange.parse(input)
                input.endsWith("falls asleep") -> FallAsleep.parse(input)
                input.endsWith("wakes up") -> WakeUp.parse(input)
                else -> throw IllegalArgumentException("Unable to parse $input")
            }
        }
    }
}

class GuardChange(date: String, hour: Int, minute: Int, val guardNo: Int) : Action(date, hour, minute) {
    constructor(vararg p: String) : this(p[1], p[2].toInt(), p[3].toInt(), p[4].toInt())

    companion object {
        private val GUARD_REGEX = Regex("""^\[(.*) (\d\d):(\d\d)] Guard #(\d*) begins shift""")
        fun parse(input: String) = GuardChange(*GUARD_REGEX.matchEntire(input)!!.groupValues.toTypedArray())
    }
}

class WakeUp(date: String, hour: Int, minute: Int) : Action(date, hour, minute) {
    constructor(vararg p: String) : this(p[1], p[2].toInt(), p[3].toInt())

    companion object {
        private val WAKE_REGEX = Regex("""^\[(.*) (\d\d):(\d\d)] wakes up""")
        fun parse(input: String) = WakeUp(*WAKE_REGEX.matchEntire(input)!!.groupValues.toTypedArray())
    }
}

class FallAsleep(date: String, hour: Int, minute: Int) : Action(date, hour, minute) {
    constructor(vararg p: String) : this(p[1], p[2].toInt(), p[3].toInt())

    companion object {
        private val SLEEP_REGEX = Regex("""^\[(.*) (\d\d):(\d\d)] falls asleep""")
        fun parse(input: String) = FallAsleep(*SLEEP_REGEX.matchEntire(input)!!.groupValues.toTypedArray())
    }
}

data class SleepPeriod(val guardNo: Int, val from: Int, val to: Int)

fun Iterable<Action>.toSleepPeriods(): Sequence<SleepPeriod> {
    var guardNo = 0
    var fellAsleep = 0
    return sequence {
        this@toSleepPeriods.forEach { action ->
            when (action) {
                is GuardChange -> {
                    guardNo = action.guardNo; fellAsleep = 0
                }
                is FallAsleep -> fellAsleep = action.minute
                is WakeUp -> {
                    yield(SleepPeriod(guardNo, fellAsleep, action.minute))
                    fellAsleep = 0
                }
            }
        }
    }
}

typealias GuardHistory = MutableMap<Int, Array<Int>>

operator fun GuardHistory.plus(sleepPeriod: SleepPeriod): GuardHistory {
    val history = this.getOrDefault(sleepPeriod.guardNo, Array(60) { 0 })
    (sleepPeriod.from until sleepPeriod.to).forEach { minute -> history[minute]++ }
    this[sleepPeriod.guardNo] = history
    return this
}

fun GuardHistory.strategy1(): Int {
    val sleepiestGuard = this.maxBy { it.value.sum() }!!
    val sleepiestMinute = sleepiestGuard.value.indexOf(sleepiestGuard.value.max())
    return sleepiestGuard.key * sleepiestMinute
}

fun GuardHistory.strategy2(): Int {
    val sleepiestGuard = this.maxBy { it.value.max()!! }!!
    val sleepiestMinute = sleepiestGuard.value.indexOf(sleepiestGuard.value.max())
    return sleepiestGuard.key * sleepiestMinute
}

fun solve(data: List<String>): Pair<Int, Int> {
    val actions = data.sorted().map { Action.parse(it) }
    val sleepPeriods = actions.toSleepPeriods()
    val guardHistory = sleepPeriods.fold(mutableMapOf<Int, Array<Int>>()) { acc, sleep ->
        acc + sleep
    }
    return Pair(guardHistory.strategy1(), guardHistory.strategy2())
}

fun main(args: Array<String>) {
    val time = measureTimeMillis {
        val (part1, part2) = solve(resourceFile("day04/guards.txt").readLines())
        println("Part 1: $part1")
        println("Part 2: $part2")
    }
    println("Elapsed Time ${time}ms")
}