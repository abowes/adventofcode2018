package jfactory.day19

import jfactory.util.resourceFile

typealias Registers = IntArray
class Instruction(val opCode: String,val a:Int,val b:Int, val c:Int){
    override fun toString() = "$opCode : $a $b $c"
    companion object {
        fun parse(line: String) : Instruction {
            val (opCode, a, b, c) = line.split(" ")
            return Instruction(opCode, a.toInt(), b.toInt(), c.toInt())
        }
    }
}
class Program(val ip: Int,val instructions: List<Instruction>){
    fun run(registers: IntArray = IntArray(6)): Int{
        while (registers[ip] in (0 until instructions.size)){
            val instruction = instructions[registers[ip]]
            operations[instruction.opCode]!!.invoke(registers, instruction)
            registers[ip] += 1
            if (ip == 28){
                println("$instruction : ${registers.joinToString(" ")}")
            }
        }
        return registers[0]
    }
}

typealias Operation = (Registers, Instruction) -> Registers

val operations: Map<String, Operation> = mapOf(
    "addr" to { r, ins -> r[ins.c] = r[ins.a] + r[ins.b];  r } ,
    "addi" to { r, ins -> r[ins.c] = r[ins.a] + ins.b; r },
    "mulr" to { r, ins -> r[ins.c] = r[ins.a] * r[ins.b]; r },
    "muli" to { r, ins -> r[ins.c] = r[ins.a] * ins.b; r },
    "banr" to { r, ins -> r[ins.c] = r[ins.a] and r[ins.b]; r },
    "bani" to { r, ins -> r[ins.c] = r[ins.a] and ins.b; r },
    "borr" to { r, ins -> r[ins.c] = r[ins.a] or r[ins.b]; r },
    "bori" to { r, ins -> r[ins.c] = r[ins.a] or ins.b; r },
    "setr" to { r, ins -> r[ins.c] = r[ins.a]; r },
    "seti" to { r, ins -> r[ins.c] = ins.a; r },
    "gtir" to { r, ins -> r[ins.c] = if (ins.a > r[ins.b]) 1 else 0; r },
    "gtri" to { r, ins -> r[ins.c] = if (r[ins.a] > ins.b) 1 else 0; r },
    "gtrr" to { r, ins -> r[ins.c] = if (r[ins.a] > r[ins.b]) 1 else 0; r },
    "eqir" to { r, ins -> r[ins.c] = if (ins.a == r[ins.b]) 1 else 0; r },
    "eqri" to { r, ins -> r[ins.c] = if (r[ins.a] == ins.b) 1 else 0; r },
    "eqrr" to { r, ins -> r[ins.c] = if (r[ins.a] == r[ins.b]) 1 else 0; r }
)


fun main(args: Array<String>) {
    val lines = resourceFile("day19/input.txt").readLines()
    val ip = lines[0].split(" ").last().toInt()
    val instructions = lines.drop(1).map { Instruction.parse(it) }
    val program = Program(ip, instructions)
    println("Part I : ${program.run()}")

    val number = 10_551_276  // Value that is assigned into Register E when Register A is initialised to 1
    val sumOfFactors = (1..number).filter { number % it == 0 }.sum()
    println("Part II: $sumOfFactors")
}