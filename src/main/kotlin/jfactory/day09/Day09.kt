package jfactory.day09

import java.util.*
import kotlin.math.absoluteValue
import kotlin.system.measureTimeMillis

fun part1(numPlayers: Int, maxMarbles: Int): Long {
    val scores = LongArray(maxMarbles)
    val marbles = CircularList<Long>(maxMarbles)
    marbles.insertAtCursor(0L)
    (1..maxMarbles).forEach { marble ->
        when {
            (marble % 23 == 0) -> {
                marbles.move(-7)
                scores[marble % numPlayers] += marbles.removeAtCursor() + marble
                marbles.move()
            }
            else -> {
                marbles.move()
                marbles.insertAtCursor(marble.toLong())
            }
        }
    }
    return scores.max()!!
}

class CircularList<E>(initialCapacity:Int){
    private val ring = ArrayDeque<E>(initialCapacity)
    fun move(steps:Int = 1) = when {
        steps > 0 -> repeat(steps) { ring.offerFirst(ring.pollLast()) }
        else -> repeat(steps.absoluteValue) { ring.offer(ring.poll()) }
    }
    fun insertAtCursor(value: E) = ring.offer(value)
    fun removeAtCursor() = ring.pollLast()!!

    fun iterator(): Iterator<E> = ring.iterator()
}


fun solve(numPlayers: Int, maxMarbles: Int): Pair<Long, Long> {
    val part1 = part1(numPlayers, maxMarbles)
    val part2 = part1(numPlayers, maxMarbles * 100)
    return part1 to part2
}


fun main(args: Array<String>) {
    val time = measureTimeMillis {
        val (part1, part2) = solve(413, 71082)
        println("Part I : $part1")
        println("Part II: $part2")
    }
    println("Elapsed Time : ${time}ms")
}


