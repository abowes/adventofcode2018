package jfactory.day16

import jfactory.util.resourceFile

typealias Registers = MutableList<Int>
typealias Instruction = List<Int>

fun Registers.addr(ins: Instruction): Registers { this[ins[3]] = this[ins[1]] + this[ins[2]]; return this }
fun Registers.addi(ins: Instruction): Registers { this[ins[3]] = this[ins[1]] + ins[2]; return this }
fun Registers.mulr(ins: Instruction): Registers { this[ins[3]] = this[ins[1]] * this[ins[2]]; return this }
fun Registers.muli(ins: Instruction): Registers { this[ins[3]] = this[ins[1]] * ins[2]; return this }
fun Registers.banr(ins: Instruction): Registers { this[ins[3]] = this[ins[1]] and this[ins[2]]; return this }
fun Registers.bani(ins: Instruction): Registers { this[ins[3]] = this[ins[1]] and ins[2]; return this }
fun Registers.borr(ins: Instruction): Registers { this[ins[3]] = this[ins[1]] or this[ins[2]]; return this }
fun Registers.bori(ins: Instruction): Registers { this[ins[3]] = this[ins[1]] or ins[2]; return this }
fun Registers.setr(ins: Instruction): Registers { this[ins[3]] = this[ins[1]]; return this }
fun Registers.seti(ins: Instruction): Registers { this[ins[3]] = ins[1]; return this }
fun Registers.gtir(ins: Instruction): Registers { this[ins[3]] = if (ins[1] > this[ins[2]]) 1 else 0; return this }
fun Registers.gtri(ins: Instruction): Registers { this[ins[3]] = if (this[ins[1]] > ins[2]) 1 else 0; return this }
fun Registers.gtrr(ins: Instruction): Registers { this[ins[3]] = if (this[ins[1]] > this[ins[2]]) 1 else 0; return this }
fun Registers.eqir(ins: Instruction): Registers { this[ins[3]] = if (ins[1] == this[ins[2]]) 1 else 0; return this }
fun Registers.eqri(ins: Instruction): Registers { this[ins[3]] = if (this[ins[1]] == ins[2]) 1 else 0; return this }
fun Registers.eqrr(ins: Instruction): Registers { this[ins[3]] = if (this[ins[1]] == this[ins[2]]) 1 else 0; return this }

val operations: Map<String, (Registers, Instruction) -> Registers> = mapOf(
    "addr" to { reg, ins -> reg.addr(ins) },
    "addi" to { reg, ins -> reg.addi(ins) },
    "mulr" to { reg, ins -> reg.mulr(ins) },
    "muli" to { reg, ins -> reg.muli(ins) },
    "banr" to { reg, ins -> reg.banr(ins) },
    "bani" to { reg, ins -> reg.bani(ins) },
    "borr" to { reg, ins -> reg.borr(ins) },
    "bori" to { reg, ins -> reg.bori(ins) },
    "setr" to { reg, ins -> reg.setr(ins) },
    "seti" to { reg, ins -> reg.seti(ins) },
    "gtir" to { reg, ins -> reg.gtir(ins) },
    "gtri" to { reg, ins -> reg.gtri(ins) },
    "gtrr" to { reg, ins -> reg.gtrr(ins) },
    "eqir" to { reg, ins -> reg.eqir(ins) },
    "eqri" to { reg, ins -> reg.eqri(ins) },
    "eqrr" to { reg, ins -> reg.eqrr(ins) }
)

data class TestCase(val before: List<Int>, val ins: Instruction, val expected: List<Int>) {
    fun registers(): Registers = before.map { it }.toMutableList()
    fun countMatchingOpCodes() = matchingOpCodes().size
    fun matchingOpCodes() : List<String> = operations.filter { it.value.invoke(registers(),ins) == expected }.map { it.key }

    companion object {
        val REGEX = Regex("""^.*\[(.*)]""")
        fun parse(lines: List<String>) : TestCase {
            val before =REGEX.matchEntire(lines[0])!!.groupValues[1].split(", ").map(String::toInt)
            val ins = lines[1].split(" ").map(String::toInt)
            val after =REGEX.matchEntire(lines[2])!!.groupValues[1].split(", ").map(String::toInt)
            return TestCase(before, ins, after)
        }
    }
}

fun part1(testCases: List<TestCase>): Int = testCases.count { it.countMatchingOpCodes() >= 3 }

fun part2(testCases: List<TestCase>, instructions:List<Instruction>): Registers {
    val opCodeMap = mapCodes(testCases)
    return instructions.fold(mutableListOf(0,0,0,0)){
            registers, ins-> operations[opCodeMap[ins[0]]]!!.invoke(registers, ins)
    }
}

/**
 * Identify the Unique Mappings of Instruction Numbers to OpCodes starting with the Uniquely Identified OpCodes
 */
fun mapCodes(testCases: List<TestCase>): Map<Int, String> {
    val possibleMappings = testCases.groupBy { it.ins[0] }
        .map { (id, testCases ) -> id to
                testCases.fold(operations.keys){ acc,testCase->
                    acc.intersect(testCase.matchingOpCodes())}.toMutableSet()
        }.toMutableList()

    val mappings = mutableMapOf<Int,String>()
    while (possibleMappings.isNotEmpty()){
        val singleton = possibleMappings.first { it.second.size == 1 }
        val opCode = singleton.second.elementAt(0)
        mappings[singleton.first] = opCode
        possibleMappings.remove(singleton) // Remove the entry
        possibleMappings.forEach { it.second.remove(opCode) }  // Remove allocated OpCode from other Sets
    }
    return mappings
}

fun main(args: Array<String>) {

    val testCases = resourceFile("day16/input.txt").readLines()
        .chunked(4)
        .takeWhile { it[0].startsWith("Before") }
        .map { lines -> TestCase.parse(lines) }

    val instructions = resourceFile("day16/input.txt").readLines()
        .drop(3190)
        .map { it.split(" ").map(String::toInt) }

    println(part1(testCases))

    println(part2(testCases, instructions))
}






