package jfactory.day15

import jfactory.util.resourceFile
import java.util.*
import kotlin.system.measureTimeMillis

enum class Direction(val dx:Int, val dy: Int){
    NORTH(0,-1), SOUTH(0,1), EAST(1,0), WEST(-1,0)
}

// Directions sorted so that they generate cells in the correct order
val directions = listOf(Direction.NORTH, Direction.WEST, Direction.EAST, Direction.SOUTH)

data class Point(val x:Int,val y:Int){
    operator fun plus(dir:Direction) = Point(this.x+ dir.dx, this.y+ dir.dy)
}

fun Point.adjacent() = directions.map { this + it }

sealed class Sprite(var position:Point, var hitPoints : Int = 200, val attackPower: Int = 3){
    fun isEnemy(other:Sprite): Boolean = other.javaClass != this.javaClass
    fun isDead() = hitPoints <= 0
    class Elf(position: Point, attackPower: Int= 3): Sprite(position, attackPower = attackPower){
        override fun toString() = "E($hitPoints)"
    }
    class Goblin(position:Point): Sprite(position){
        override fun toString() = "G($hitPoints)"
    }
    fun takeDamage(attackPower: Int){ this.hitPoints -= attackPower }
}

typealias MapLayout = Array<CharArray>
fun MapLayout.isOpen(x:Int,y:Int) = this[y][x] != '#'
fun MapLayout.isOpen(pos: Point) = this.isOpen(pos.x,pos.y)
fun MapLayout.asString(sprites:List<Sprite>) : String {
    return this.mapIndexed { y, chars ->  chars.mapIndexed{ x,c ->
        when (c){
            '#' -> '#'
            else -> if (sprites.isOccupied(Point(x,y))) 'X' else '.'
        }
    }.joinToString("")}.joinToString("\n")
}

fun List<Sprite>.isOccupied(pos:Point) = this.any { !it.isDead() && it.position == pos }
fun List<Sprite>.enemies(sprite: Sprite) = this.filter{!it.isDead() && it.isEnemy(sprite)}

fun Sprite.shortestPathToEnemy(mapLayout: MapLayout, sprites:List<Sprite>) : List<Point>? {
    val targetPositions = sprites.enemies(this).flatMap { it.position.adjacent() }
        .filter { mapLayout.isOpen(it) }

    // Check if we are already next to an enemy, if so we don't need to move & so return an empty list
    if (this.position in targetPositions){
        return emptyList()
    }

    val visited = mutableListOf(this.position)
    val paths = ArrayDeque<List<Point>>()
    this.position.adjacent()
        .filter { mapLayout.isOpen(it) && !sprites.isOccupied(it) }
        .forEach { paths.offer(listOf(it)) }
    while (!paths.isEmpty()){
        val path = paths.poll()!!
        val end = path.last()
        if (end in visited){
            continue
        }
        visited.add(end)
        if (end in targetPositions){
            return path
        }

        end.adjacent()
                .filter { (it !in visited) && mapLayout.isOpen(it) && !sprites.isOccupied(it) }
                .forEach { paths.offer( listOf(path, listOf(it)).flatten()) }

    }

    // If there is no path then return Null
    return null
}

fun Sprite.performAction(mapLayout: MapLayout, sprites: List<Sprite>){
    val path = this.shortestPathToEnemy(mapLayout,sprites)
    if (path != null){
        // Move
        if (!path.isEmpty()){
            this.position = path.first()
        }
        // Attack if Adjacent to Enemy
        val enemies: List<Sprite> = this.adjacentEnemies(sprites)
        if (enemies.isNotEmpty()){
            val enemy = enemies.sortedWith(compareBy({it.hitPoints},{it.position.y},{it.position.x})).first()
            enemy.takeDamage(this.attackPower)
        }
    }
}

private fun Sprite.adjacentEnemies(sprites: List<Sprite>)= sprites.enemies(this).filter { this.position in it.position.adjacent() }

fun part1(mapLayout:MapLayout) : Int{
    val sprites = generateSprites(mapLayout)
    val loopCount = playGame(sprites, mapLayout)
    return loopCount * sprites.sumBy { it.hitPoints }
}

private fun playGame(sprites: MutableList<Sprite>, mapLayout: MapLayout): Int {
    var loopCount = 0
    while (sprites.enemies(sprites.first()).isNotEmpty()) {
        sprites.sortWith(compareBy({ it.position.y }, { it.position.x }))
        sprites.forEach { sprite ->
            if (!sprite.isDead()) {
                sprite.performAction(mapLayout, sprites)
            }
        }
        sprites.removeIf { it.isDead() }
        if (sprites.enemies(sprites.first()).isNotEmpty()) {
            loopCount++
        }
    }
    return loopCount
}

fun part2(mapLayout:MapLayout): Pair<Int, Int> {
    val elfCount = generateSprites(mapLayout).filter { it is Sprite.Elf }.count()
    return generateSequence(4){it+1}
        .map { elfAttack -> remainingElves(mapLayout, elfAttack) }
        .first { it.first == elfCount}
}

private fun remainingElves(mapLayout: MapLayout, elfAttack: Int): Pair<Int, Int> {
    val sprites = generateSprites(mapLayout, elfAttack)
    val loopCount = playGame(sprites, mapLayout)
    val remainingElves = sprites.filter { it is Sprite.Elf }.count()
    return remainingElves to loopCount * sprites.sumBy { it.hitPoints }
}

fun solve(mapLayout: MapLayout): Pair<Int, Int> {
    val part1 = part1(mapLayout)
    val part2 = part2(mapLayout)
    return part1 to part2.second
}

fun main(args: Array<String>) {
    val mapLayout: MapLayout = resourceFile("day15/test5.txt").readLines().map{it.toCharArray()}.toTypedArray()
    val time = measureTimeMillis {
        val (part1, part2) = solve(mapLayout)
        println("Part I: $part1")
        println("Part II: $part2")
    }
    println("Elapsed Time: ${time}ms")
}

private fun generateSprites(mapLayout: MapLayout, elfAttackPoints:Int = 3): MutableList<Sprite> {
    println("Elf Attack: $elfAttackPoints")
    return mapLayout.mapIndexed { y, chars ->
        chars.mapIndexed { x, c ->
            when (c) {
                'E' -> Sprite.Elf(Point(x, y), elfAttackPoints)
                'G' -> Sprite.Goblin(Point(x, y))
                else -> null
            }
        }
    }.flatten()
        .filterNotNull()
        .toMutableList()
}