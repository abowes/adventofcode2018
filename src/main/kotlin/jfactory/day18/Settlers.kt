package jfactory.day18

import jfactory.util.Coord
import jfactory.util.Grid2D
import jfactory.util.read2DArrayFile
import java.lang.IllegalArgumentException
import kotlin.system.measureTimeMillis

const val OPEN = '.'
const val LUMBER = '#'
const val TREES = '|'

class Woods(elements:Array<CharArray>): Grid2D(elements) {

    fun neighbours(coord: Coord) = coord.neighbours()
        .filter{ it.x in (0 until width) && it.y in (0 until height)}
        .groupingBy { this[it] }.eachCount()
    fun neighbours(x:Int, y:Int) : Map<Char,Int> = neighbours(Coord(x,y))

    /**
     * An open acre will become filled with trees if three or more adjacent acres contained trees. Otherwise, nothing happens.
     * An acre filled with trees will become a lumberyard if three or more adjacent acres were lumberyards. Otherwise, nothing happens.
     * An acre containing a lumberyard will remain a lumberyard if it was adjacent to at least one other lumberyard
     * and at least one acre containing trees. Otherwise, it becomes open.
     */
    fun nextChar(x:Int, y:Int): Char {
        val neighbours = neighbours(x,y)
        return when (this[x,y]){
            OPEN -> if (neighbours.getOrDefault(TREES,0) >= 3) TREES else OPEN
            TREES -> if (neighbours.getOrDefault(LUMBER,0) >= 3) LUMBER else TREES
            LUMBER -> if (neighbours.getOrDefault(LUMBER,0) >= 1
                && neighbours.getOrDefault(TREES,0) >= 1) LUMBER else OPEN
            else -> throw IllegalArgumentException("Unexpected Character : ${this[x,y]}")
        }
    }

    fun nextGeneration() : Woods {
        return Woods((0 until height).map{ y ->
            (0 until width).map{ x-> nextChar(x,y)}.toCharArray()
        }.toTypedArray())
    }

    fun resourceValue() : Int {
        val resources = elements.flatMap { it.asIterable() }.groupingBy { it }.eachCount()
        return resources.getOrDefault(TREES, 0) * resources.getOrDefault(LUMBER, 0)
    }

}

fun part1(woods: Woods) = (0 until 10).fold(woods){ layout, _ -> layout.nextGeneration()}.resourceValue()

/**
 * n is a very big number so look for repeated layouts
 */
fun part2(woods: Woods, n: Long) : Int {
    val hashCodes = mutableListOf<Int>()
    val resourceValues = mutableListOf<Int>()
    var layout = woods
    while (layout.hashCode !in hashCodes){
        hashCodes.add(layout.hashCode)
        resourceValues.add(layout.resourceValue())
        layout = layout.nextGeneration()
    }
    val cyclePrefix = hashCodes.indexOf(layout.hashCode)
    val cycleLength = hashCodes.size - cyclePrefix
    return resourceValues[((n - cyclePrefix) % cycleLength).toInt() + cyclePrefix]
}

fun main(args: Array<String>) {
    val woods = Woods(read2DArrayFile("day18/input.txt"))
    val time = measureTimeMillis {
        println("Part I : ${part1(woods)}")
        println("Part II: ${part2(woods, 1_000_000_000L)}")
    }
    println("Elapsed Time : ${time}ms")

}
