package jfactory.day20

import jfactory.util.resourceFile
import java.util.*

//val DOOR_EW = '|'
//val DOOR_NS = '-'
//val WALL = '#'
//val OPEN = '.'
//val UNKNOWN = '?'

//sealed class TreeNode(val parent: TreeNode?){
//    val chars = StringBuffer()
//    fun addChild() = ChildNode(this)
//    fun addSibling() = this.parent?.addChild() ?: throw IllegalAccessException("Cant' add Sibling to Root Node")
//    operator fun plus(c: Char) { chars.append(c)}
//
//    class RootNode : TreeNode(null)
//    class ChildNode(parent: TreeNode) : TreeNode(parent)
//
//    companion object {
//        fun parse(input: CharSequence){
//            var currentNode = RootNode()
//
//        }
//    }
//}

enum class Direction(val dx:Int, val dy:Int){
    N(0,-1), S(0,1), E(1,0), W(-1,0);

    operator fun unaryMinus() = when (this){
        N -> S
        S -> N
        E -> W
        W -> E
    }
}
fun Char.toDirection() = when (this) {
    'N' -> Direction.N
    'S' -> Direction.S
    'E' -> Direction.E
    'W' -> Direction.W
    else -> throw IllegalArgumentException("Invalid Direction : $this")
}

data class Point(val x:Int, val y: Int){
    operator fun plus(dir: Direction) = Point(x+dir.dx, y+dir.dy)
}


//class Room(val location : Pair<Int,Int> ) {
//    constructor(x:Int,y:Int) : this(x to y)
//    val id = location.toId()
//    val linkedRooms = mutableMapOf<Direction, Int>()
//    fun addLink(dir: Direction, room: Room) {addLink(dir, room.id)}
//    fun addLink(dir: Direction, roomId: Int) {
//        linkedRooms[dir] = roomId
//    }
//}
//
//fun Pair<Int,Int>.toId() = (10000 * this.second) + this.first
//operator fun Pair<Int,Int>.plus(dir:Direction) = (this.first + dir.dx) to (this.second + dir.dy)
//
//class RoomLayout(){
//    private val rooms = mutableMapOf<Int,Room>(0 to Room(0,0))
//
//    operator fun get(id: Int) = rooms[id]
//    fun move( from: Room, dir: Char) = move(from, dir.toDirection())
//    fun move( from: Room, dir: Direction) : Room {
//        val nextLocation = from.location + dir
//        val nextRoom = rooms.getOrPut(nextLocation.toId()){Room(nextLocation)}
//        from.addLink(dir, nextRoom)
//        nextRoom.addLink(-dir, from)
//        return nextRoom
//    }
//
//    companion object {
//        fun parse(input: CharSequence) : RoomLayout {
//
//            val stack = ArrayDeque<Int>(1000)
//            val roomLayout = RoomLayout()
//            var currentRoom = roomLayout[0]!!
//
//            input.forEach {c ->
//                when (c) {
//                    'N','S', 'E', 'W' -> currentRoom = roomLayout.move(currentRoom, c)
//                    '(' -> stack.push(currentRoom.id)
//                    ')' -> stack.pop()
//                    '|' -> { roomId =
//
//                    }
//                }
//            }
//        }
//    }
//
//}

fun generateDistanceMap(path:String): Map<Point,Int>{

    var currentPoint = Point(0,0)
    val distanceMap = mutableMapOf(currentPoint to 0)

    val stack = ArrayDeque<Point>(1000)
    path.forEach { c ->
        val nextPoint = when (c) {
            '(' -> {stack.push(currentPoint); currentPoint}
            ')' -> stack.pop()!!
            '|' -> stack.peek()!!
            'N', 'S', 'E', 'W' -> currentPoint + c.toDirection()
            else -> currentPoint
        }
        distanceMap[nextPoint] = minOf(distanceMap.getOrDefault(nextPoint, Int.MAX_VALUE), distanceMap[currentPoint]!! + 1)
        currentPoint = nextPoint
    }

    return distanceMap
}


fun solve(path: String) : Pair<Int,Int>{

    val distanceMap = generateDistanceMap(path)

    return distanceMap.values.max()!! to distanceMap.values.count { it >= 1000 }
}

fun main(args: Array<String>) {
    val path = resourceFile("day20/input.txt").readLines().first()

    val (part1, part2) = solve(path)

    println("Part I : $part1")
    println("Part II : $part2")

}