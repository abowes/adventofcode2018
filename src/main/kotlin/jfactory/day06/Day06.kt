package jfactory.day06

import jfactory.util.resourceFile
import java.lang.Math.abs
import kotlin.system.measureTimeMillis

data class Point(val x: Int, val y: Int) {
    operator fun minus(other: Point) = abs(this.x - other.x) + abs(this.y - other.y)
}

fun Point.closest(points: List<Point>): Int {
    val closestPoints = points.mapIndexed { index, point -> index to (point - this) }
        .sortedBy { it.second }
        .take(2)
    return if (closestPoints[0].second == closestPoints[1].second) -1 else closestPoints[0].first
}

fun List<Point>.getCorners(margin: Int = 0): Pair<Point, Point> {
    val minX = this.minBy { it.x }!!.x
    val minY = this.minBy { it.y }!!.y
    val maxX = this.maxBy { it.x }!!.x
    val maxY = this.maxBy { it.y }!!.y
    return Point(minX - margin, minY - margin) to Point(maxX + margin, maxY + margin)
}

/**
 * Grid containing the Indexes of the Closest Points
 */
fun List<Point>.toGrid(): List<List<Int>> {
    val (top, bottom) = this.getCorners()
    return (top.y..bottom.y).map { y -> (top.x..bottom.x).map { x -> Point(x, y).closest(this) } }
}

fun List<Point>.centre(): Point {
    val corners = this.getCorners()
    return Point((corners.first.x + corners.second.x) /2,(corners.first.y + corners.second.y) /2 )
}

/**
 * Generate the set of points which form a boundary enclosing the known points
 */
fun List<Point>.boundaryPoints(border: Int = 0): List<Point> {
    val (top, bottom) = this.getCorners(border)
    return listOf((top.y..bottom.y).flatMap { y -> listOf(top.x, bottom.x).map { x -> Point(x, y) } },
        listOf(top.y, bottom.y).flatMap { y -> (top.x..bottom.x).map { x -> Point(x, y) } }).flatten()
}

/**
 * Find the Indexes of the Points which are Infinite
 * This points will be the ones which touch a boundary a reasonable distance away
 */
fun List<Point>.infinitePoints(): List<Int> {
    return this.boundaryPoints(0).map { it.closest(this) }.distinct()
}

fun List<Point>.safeRegion(totalDistance: Int): List<Point> {
    val (top, bottom) = this.getCorners(20)
    return (top.y..bottom.y).map { y -> (top.x..bottom.x).map { x -> Point(x, y) } }
        .flatten().map { point -> point to this.map { point - it }.sum() }
        .filter { it.second < totalDistance }.map { it.first }
}

fun solve(input: List<Point>): Pair<Int, Int> {

    val infiniteIndexes = input.infinitePoints()
    val grid = input.toGrid()

    val largestArea = grid.flatten().groupBy { it }.filterNot { it.key in infiniteIndexes }
        .map { it.key to it.value.size }.maxBy { it.second }!!

    val safeZone = input.safeRegion(10000)
    return Pair(largestArea.second, safeZone.size)
}


fun main(args: Array<String>) {
    repeat(10) {
        val time = measureTimeMillis {
            val (part1, part2) = solve(resourceFile("day06/input.txt").readLines()
                .map { it.split(", ").map(String::toInt) }
                .map { Point(it[0], it[1]) })
            println("Part I: $part1")
            println("Part II: $part2")
        }
        println("Elapsed Time ${time}ms")
    }
}
