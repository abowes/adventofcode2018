package jfactory.day05

import jfactory.util.resourceFile
import kotlin.system.measureTimeMillis

fun CharSequence.react(): String {
    return this.fold(mutableListOf<Char>()){
        acc, c -> when( acc.getOrNull(0) matches c) {
                true -> {acc.removeAt(0); acc}
                false -> {acc.add(0,c);acc}
    }
    }.reversed().joinToString(separator = "")
}

private infix fun Char?.matches(other:Char) = when {
        this == null -> false
        this.toUpperCase() != other.toUpperCase() -> false
        this.isUpperCase() == other.isUpperCase() -> false
        else -> true
}

fun part1(polymer: String) = polymer.react()
fun part2(polymer: String) = ('a'..'z').map { c -> polymer.filterNot { c.equals(it,ignoreCase = true) }.react() }.minBy(String::length)!!

fun solve(polymer: String): Pair<Int, Int> {
    val basePolymer = part1(polymer)
    return Pair(basePolymer.length, part2(basePolymer).length)
}

fun main(args: Array<String>) {
    val time = measureTimeMillis {
        val polymer = resourceFile("day05/polymer.txt").readLines().first()
        val (part1, part2) = solve(polymer)
        println("Part 1: $part1")
        println("Part 2: $part2")
    }
    println("Elapsed Time ${time}ms")
}