package jfactory.util

enum class Direction(val dx:Int, val dy: Int){
    UP(0,-1), RIGHT(1,0), DOWN(0,1), LEFT(-1,0),
    UP_LEFT(-1,-1), UP_RIGHT(1,-1), DOWN_LEFT(-1,1), DOWN_RIGHT(1,1);

    fun cardinalDirections() = listOf(UP, LEFT, DOWN, RIGHT)
    fun diagonals() = listOf(UP_LEFT, UP_RIGHT, DOWN_LEFT, DOWN_RIGHT)
}

class Coord(val x:Int, val y:Int){
    operator fun plus(dir:Direction) = Coord(x + dir.dx, y + dir.dy)
    fun neighbours() : List<Coord> = Direction.values().map { this + it }
}

open class Grid2D(val elements: Array<CharArray>){
    val width = elements[0].size
    val height = elements.size

    operator fun get(coord: Coord) = elements[coord.y][coord.x]
    operator fun get(x:Int, y:Int) = elements[y][x]
    operator fun set(coord: Coord, v: Char) {
        elements[coord.y][coord.x] = v
    }
    operator fun set(x:Int,y:Int, v: Char) { elements[y][x] = v }

    val hashCode : Int by lazy{this.elements.map{ it.joinToString("")}.joinToString("").hashCode()}
}