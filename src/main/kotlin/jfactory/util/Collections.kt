package jfactory.util

/**
 * Reverse a section within a Mutable List
 * This function treats the List as a Circular List so that the flipped region can overlap the end of the list.
 */
fun <E> MutableList<E>.flip(start: Int, count: Int)  : MutableList<E>{
    val idx = (start until start + count).map { it % this.size }
    this.slice(idx).reversed().zip(idx).forEach {
        this[it.second] = it.first
    }
    return this
}

/**
 * Converts an Iteration to an Infinitely repeating Sequence.
 *
 * If the Iteration is empty then an empty sequence will be returned.
 */
fun <E> Iterable<E>.cycle(): Sequence<E> = sequence {
    if (this@cycle.none()){
        return@sequence
    } else {
        while (true) {
            yieldAll(this@cycle)
        }
    }
}

fun List<String>.to2DArray()= this.map { it.toCharArray() }.toTypedArray()

/**
 * Accumulate Function
 *
 * Basically a Combination of Fold and Map which uses the result of previous element to act as the entry into the next.
 */
fun <T, R> Sequence<T>.accumulate(initial: R, operation: (acc: R, T) -> R): Sequence<R> = sequence {
    var accumulator = initial
    forEach {
        accumulator = operation(accumulator, it)
        yield(accumulator)
    }
}

/**
 * Memoization which is backed by a Mutable Map
 * Provide a function which will be used to populate the missing elements as they are requested.
 */
class MemoizedMap<K,V>(val items: MutableMap<K,V>, private val memoFunction : (K) -> V) : MutableMap<K,V> by items {
    override fun get(key: K): V {
        val value = items.get(key)
        return if (value == null) {
            val answer = memoFunction.invoke(key)
            items.put(key, answer)
            answer
        } else {
            value
        }
    }
}


fun <K, V> MutableMap<K, MutableList<V>>.addItem(key: K, value: V) {
    val item = this.getOrDefault(key, mutableListOf())
    item.add(value)
    this[key] = item
}

fun <K, V> MutableMap<K, MutableList<V>>.removeItem(key: K, value: V) {
    val l = this[key]
    if (l != null) {
        l.remove(value)
        when (l.isEmpty()) {
            true -> this.remove(key)
            else -> this[key] = l
        }
    }
}