package jfactory.util

import java.io.File

fun resourceFile(name: String) = File(ClassLoader.getSystemResource(name).file)

fun read2DArrayFile(name:String) = resourceFile(name).readLines().to2DArray()

