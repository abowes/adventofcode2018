package jfactory.day10

import jfactory.util.accumulate
import jfactory.util.resourceFile

class Star(val x: Int, val y : Int, val vx: Int, val vy: Int){
    operator fun inc() : Star = Star(x + vx, y + vy, vx, vy)
}

fun List<Star>.boundingSize():Int = (this.maxBy { it.x }!!.x - this.minBy { it.x }!!.x) + (this.maxBy { it.y }!!.y - this.minBy { it.y }!!.y)
fun List<Star>.contains(x:Int, y: Int) = this.any { it.x == x && it.y == y }

fun List<Star>.asString() = (this.minBy{ it.y }!!.y .. this.maxBy{ it.y }!!.y).map { y->
        (this.minBy{ it.x }!!.x .. this.maxBy{ it.x }!!.x).map{
            x -> if (this.contains(x,y)) "#" else " "
        }.joinToString(separator = "")
    }.joinToString(separator = "\n")

fun solve(stars:List<Star>) : Pair<String, Int> {

    val (i, closest) = generateSequence(1){ it + 1 }.accumulate(0 to stars){
        acc,loop-> loop to acc.second.map { s-> s.inc() }
    }
        .dropWhile { it.second.boundingSize() > 100 }  // Only consider when stars converge
        .takeWhile { it.second.boundingSize() < 100 }  // Stop when stars start to diverge
        .minBy { it.second.boundingSize() }!!

    return closest.asString() to i
}


fun main(args: Array<String>) {
    val STAR_REGEX = Regex("""^position=< *(-?\d*), *(-?\d*)> velocity=< *(-?\d*), *(-?\d*)>""")
    val stars = resourceFile("day10/stars.txt").readLines().map {
        val (x, y, vx, vy) = STAR_REGEX.matchEntire(it)!!.groupValues.drop(1).map(String::toInt)
        Star(x,y,vx,vy)
    }

    val (part1, part2) = solve(stars)
    println("Part1:\n$part1\n")
    println("Part2: $part2")
}